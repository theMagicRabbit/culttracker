import sys
import mariadb

try:
    conn = mariadb.connect(
            host="localhost",
            user="",
            password="")
except mariadb.Error as e:
    print(f"Jezebel has murdered the database connection: {e}")
    sys.exit(1)

cur = conn.cursor()
